<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <title>Standings</title>
    <style>
        .transition {
            transition: all .8s ease-in-out;
        }

        /* Mozilla & Chrome */
        .hide-scroll::-webkit-scrollbar {
            display: none;
        }

        /* Edge IE */
        .hide-scroll {
            -ms-overflow-style: none;
        }

        /* .bg_cl{
            background-image: url(https://media.contentapi.ea.com/content/dam/ea/fifa/fifa-19/images/2019/03/fifa19-hero-large-uefa-champions-league-16x9-xl.jpg.adapt.crop3x5.320w.jpg);
            background-color: #7d7da5;
            background-size: cover;
            background-blend-mode: multiply;
            background-origin: content-box;
            background-position: center;
             */
        /* } */
    </style>
</head>

<body class="w-screen flex justify-start hide-scroll">
    <div class="w-full h-full md:w-1/2 flex flex-col justify-center p-4 overflow-y-scroll hide-scroll">

        @foreach($groups as $group)
        <span
            class="text-md md:text-2xl text-blue-600 cursor-pointer mt-2 md:mt-8 hover:text-white-400 transition">
            <a href="{{route('getStandings',['id'=>$group->id])}}">{{$group->name}}</a></span>
        <div class="border w-full rounded-lg flex md:mt-1">

            <table class="table-fixed w-full">
                <thead>
                    <tr>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6"></th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-24 lg:w-48">Team</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">MP</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">W</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">D</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">L</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">GF</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">GA</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">GD</th>
                        <th class="text-xs md:text-md font-light md:font-bold py-1 w-6 lg:w-8">Pts</th>
                    </tr>
                </thead>
                <tbody>
                    <span class="hidden">{{$index = 1}}</span>

                    @foreach($group->teams as $key=>$team)
                    <tr
                        class="{{$index == 1 ? 'border-0 border-l-2 border-blue-600':''}} {{$index == 2 ? 'border-0 border-l-2 border-blue-600':''}} {{$index == 3 ? 'border-0 border-l-2 border-orange-400':''}} {{$index == 4 ? 'border-0 border-l-2 border-transparent':''}} ">
                        <td class="p-1">
                            {{-- <img class="w-4" src="{{$team->logo}}" alt=""> --}}
                            <img class="w-6" src="{{$team->logo}}" alt="">
                        </td>
                        <td class="text-xs md:text-md font-bold  py-1">{{ $team->name }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->mp }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->w }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->d }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->l }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->gf }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->ga }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->gd }}</td>
                        <td class="text-xs md:text-md font-light md:font-base  py-1 text-center">{{ $team->pts }}</td>
                    </tr>
                    <span class="hidden">{{ $index++}}</span>
                    @endforeach
                </tbody>
            </table>
        </div>

        @endforeach
    </div>


    <div class="w-1/2 h-screen bg-blue-700 hidden md:flex fixed right-0 overflow-y-auto hide-scroll pt-4"
        style="display: flex; flex-direction: column;padding:1rem">
        @if($matches ?? '')
        <span class="hidden">{{$index2 = 1}}</span>
        @foreach ($matches ?? '' as $match)
        <h4 class=" lg:text-sm text-left text-white text-xs leading-none">{{$match['played_at']}}</h4>
        <div style="display: grid;grid-template-columns:5% 30% 10% 4% 2% 4% 10% 30% 5%;grid-template-rows:10px 30px 10px;">

            <div style="grid-column: 1/2;grid-row: 2/3"></div>

            <div class="text-base font-bold"
             style="border-radius: 15px; background:white;grid-column: 2/3;grid-row: 2/3;text-align: center;align-self: center">{{$match['team1']}}</div>
            <div style="grid-column: 3/4;grid-row: 1/4;align-self: center;place-self: center"><img class="w-8" src="{{$match['logo1']}}" alt=""></div>
            <div class="p-1 " style="grid-column: 4/5;grid-row: 2/3;text-align: center;font-weight: 900;align-self: center;background:white;border-radius:12px">{{$match['score_1']}}</div>

        <div class="" style="grid-column: 5/6;grid-row:2/3;text-align: center;font-weight: 900;align-self: center;"></div>

            <div class="p-1 " style="grid-column: 6/7;grid-row: 2/3;text-align: center;font-weight: 900;align-self: center;background:white;border-radius:12px">{{$match['score_2']}}</div>
            <div style="grid-column: 7/8;grid-row: 1/4;align-self: center;place-self: center"><img class="w-8" src="{{$match['logo2']}}" alt=""></div>
            <div class="text-base font-bold" 
            style="border-radius: 15px;background:white;grid-column: 8/9;grid-row: 2/3;text-align: center;align-self: center">{{$match['team2']}}</div>

            <div style="grid-column: 9/10;grid-row: 2/3"></div>
    </div>
    <span class="hidden">{{ $index2++}}</span>
    @endforeach
    @endif
    </div>

</body>

</html>