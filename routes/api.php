<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::resource('groups', 'GroupController')->except('create', 'edit');
// Route::resource('teams', 'TeamController')->except('create', 'edit');
Route::get('store-standings', 'StandingController@storeStandings')->name('storeStandings');
Route::get('get-standings', 'StandingController@getStandings')->name('getStandings');
Route::get('get-matches/{id}', 'StandingController@getMatchesPerGroup')->name('getMatches');

Route::post('r16-matches', 'MatchController@r16Matches')->name('r16Matches');
Route::post('r16-matches-starts', 'MatchController@r16MatchesStarts')->name('r16MatchesStarts');
