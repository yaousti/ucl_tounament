<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('team_1');
            $table->unsignedBigInteger('team_2');
            $table->unsignedInteger('score_1')->nullable();
            $table->unsignedInteger('score_2')->nullable();
            $table->unsignedInteger('score_extra_1')->nullable();
            $table->unsignedInteger('score_extra_2')->nullable();
            $table->unsignedInteger('penalties_1')->nullable();
            $table->unsignedInteger('penalites_2')->nullable();
            $table->string('phase')->nullable();
            $table->timestamp('played_at')->nullable();
            $table->timestamps();

            $table->foreign('team_1')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->foreign('team_2')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
