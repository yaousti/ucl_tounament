<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('season');
            $table->unsignedBigInteger('team_id');
            $table->integer('mp')->nullable()->default(0);
            $table->integer('w')->nullable()->default(0);
            $table->integer('d')->nullable()->default(0);
            $table->integer('l')->nullable()->default(0);
            $table->integer('gf')->nullable()->default(0);
            $table->integer('ga')->nullable()->default(0);
            $table->integer('gd')->nullable()->default(0);
            $table->integer('pts')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standings');
    }
}
