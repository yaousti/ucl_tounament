<?php

use App\Group;
use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = File::get("database/data/groups.json");
        $groups = json_decode($data);
        $groups = $groups->groups;
        
        foreach ($groups as $group) {
            Group::create([
                'name' => $group->name
            ]);
        }
    }
}
