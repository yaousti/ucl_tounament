<?php

use App\Team;
use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/groups.json");
        $data = json_decode($json);
        $data = $data->groups;

        foreach ($data as $d) {
            for ($i = 0; $i < 4; $i++) {
                Team::create([
                    'name' => $d->teams[$i]->name,
                    'group_id' => $d->id,
                    'logo' => $d->teams[$i]->logo
                ]);
            }
        }
    }
}
