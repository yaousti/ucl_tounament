<?php

use App\Match;
use Illuminate\Database\Seeder;

class MatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/matchs.json");
        $data = json_decode($json);
        $data = $data->matchs;

        foreach ($data as $d) {
            Match::create([
                'team_1' => $d->team_1,
                'team_2' => $d->team_2,
                'score_1' => $d->score_1,
                'score_2' => $d->score_2,
                'phase' => $d->phase,
                'played_at' => $d->played_at
            ]);
        }
    }
}
