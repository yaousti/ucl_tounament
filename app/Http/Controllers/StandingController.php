<?php

namespace App\Http\Controllers;

use App\Group;
use App\Match;
use App\Qualifier;
use App\Standing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StandingController extends Controller
{
    public function storeStandings(Request $request)
    {
        $current_season = Carbon::now();
        $season = '';

        if ($request->season) {
            $date = explode('/', $request->season);
            $start_date = $date[0] . '-09-10';
            $end_date = $date[1] . '-06-30';

            Carbon::parse($start_date)->format('Y-m-d');
            Carbon::parse($end_date)->format('Y-m-d');
            $matches = Match::where('phase', 'groups')->whereBetween('played_at', [$start_date, $end_date])->get();
            $season = $date[0] . '/' . $date[1];
            if ($matches->count() == 0) {
                return response()->json([
                    'warning' => 'could not find any data for this season'
                ]);
            }
        } else {
            $matches = Match::where('phase', 'groups')->whereBetween('played_at', [$current_season->year . '-09-10', Carbon::now()])->get();
            $season = $current_season->year . '/' . $current_season->addYear()->year;
            if ($matches->count() == 0) {
                return response()->json([
                    'warning' => 'could not find any data for neither the requested or the current season'
                ]);
            }
        }

        foreach ($matches as $match) {

            $stats_1 = Standing::where('team_id', $match->team_1)->first();
            $stats_2 = Standing::where('team_id', $match->team_2)->first();

            if ($stats_1 == false) {
                $stats_1 = new Standing;
                $stats_1->team_id = $match->team_1;
                $stats_1->season = $season;
            }

            if ($stats_2 == false) {
                $stats_2 = new Standing;
                $stats_2->team_id = $match->team_2;
                $stats_2->season = $season;
            }
            if ($stats_1->mp == 6 && $stats_2->mp == 6) {
                continue;
            }
            if ($match->score_1 == $match->score_2) {
                $stats_1->d += 1;
                $stats_2->d += 1;
                $stats_1->pts += 1;
                $stats_2->pts += 1;
            } else if ($match->score_1 > $match->score_2) {
                $stats_1->w += 1;
                $stats_2->l += 1;
                $stats_1->pts += 3;
            } else {
                $stats_1->l += 1;
                $stats_2->w += 1;
                $stats_2->pts += 3;
            }

            $stats_1->gf += $match->score_1;
            $stats_1->ga += $match->score_2;

            $stats_2->gf += $match->score_2;
            $stats_2->ga += $match->score_1;

            $stats_1->save();
            $stats_2->save();
        }
        $global_stats = Standing::paginate(4);
        return response()->json(['stats' => $global_stats]);
    }

    public function getStandings()
    {
        $groups = Group::all();
        $teams = DB::select(DB::raw("select teams.id, teams.group_id, teams.name,teams.logo, season, mp, w, d, l, gf, ga, gd, pts
        from homestead.teams inner join homestead.standings on teams.id = standings.team_id 
        order by standings.pts desc, standings.gd desc, standings.gf desc;"));

        $qualifiers = [];

        $teams = collect($teams);
        $groups = $groups->map(function ($group) use ($teams, &$qualifiers) {
            $count = 0;
            $qualifiers[$group->id] = [];
            $group->teams = $teams->filter(function ($team) use ($group, &$qualifiers, &$count) {
                if ($group->id == $team->group_id && $count < 2) {
                    array_push($qualifiers[$group->id], $team);
                    $count++;
                }
                return $group->id == $team->group_id;
            });
            return $group;
        });

        foreach ($qualifiers as $teams) {
            foreach ($teams as $team) {
                $qualifier = Qualifier::all();
                if (count($qualifier) < 16) {
                    $qualifier = new Qualifier;
                    $qualifier->team_id = $team->id;
                    $qualifier->round = 'round of 16';
                    $qualifier->season = $team->season;
                    $qualifier->save();
                }
            }
        }
        return response()->json([
            'groups' => $groups
        ]);
    }
    public function getMatchesPerGroup($id)
    {
        $matches = [];
        $group = Group::whereId($id)->with(['teams' => function ($q) {
            $q->with(['match_home' => function ($q) {
                $q->with(['team_home', 'team_away']);
            }]);
        }])->get()->toArray();
        $key = 0;
        foreach ($group[0]['teams'] as $team) {

            foreach ($team['match_home'] as $match) {
                if ($match['phase'] == 'groups') {

                    $matches[$key]['played_at'] = $match['played_at'];
                    $matches[$key]['team1'] = $match['team_home']['name'];
                    $matches[$key]['logo1'] = $match['team_home']['logo'];
                    $matches[$key]['score_1'] = $match['score_1'];
                    $matches[$key]['team2'] = $match['team_away']['name'];
                    $matches[$key]['score_2'] = $match['score_2'];
                    $matches[$key]['logo2'] = $match['team_away']['logo'];

                    $key += 1;
                }
            }
        }

        usort($matches, function ($a, $b) {
            $t1 = strtotime($a['played_at']);
            $t2 = strtotime($b['played_at']);
            return $t1 - $t2;
        });

        return response()->json([
            'matches' => $matches,
        ]);

        // return view('standings', compact('matches', 'groups'));


        // return view("standings", compact('groups'));
    }
}
