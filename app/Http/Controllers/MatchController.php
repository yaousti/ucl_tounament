<?php

namespace App\Http\Controllers;

use App\Group;
use App\Match;
use App\Qualifier;
use App\Team;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function r16Matches()
    {
        $qualifier = Qualifier::where('round', 'round of 16')->get();
        $teams = $qualifier->load('team')->toArray();

        // $matches = [];
        $matches = Match::where('phase', 'round of 16')->get();
        if (sizeof($matches) != 16) {


            $j = sizeof($teams) - 1;
            for ($i = 0; $i < sizeof($teams) / 2; $i++) {

                $match = new Match;
                $match->team_1 = $teams[$i]['team']['id'];
                $match->team_2 = $teams[$j]['team']['id'];
                $match->phase = $teams[$i]['round'];
                $match->save();
                // array_push($matches, $match);
                $j--;
            }

            $j = 0;
            for ($i = sizeof($teams) - 1; $i > (sizeof($teams) / 2) - 1; $i--) {
                $match = new Match;
                $match->team_1 = $teams[$i]['team']['id'];
                $match->team_2 = $teams[$j]['team']['id'];
                $match->phase = $teams[$i]['round'];
                $match->save();
                // array_push($matches, $match);
                $j++;
            }
            return response()->json(['message' => 'matches has been created successfully !']);
        }
        return response()->json(['message' => 'matches was already plannified !']);
    }

    public function r16MatchesStarts(Request $request)
    {
        $matches = Match::where('phase', 'round of 16')->get();

        $match = Match::find($request->id);
        dd($request[0]['id']);

        if ($match->score_1 == null &&  $match->score_2 == null) {
            $match->score_1 = $request->input('score_1');
            $match->score_2 = $request->input('score_2');

            if ($request->input('score_extra_1')) {
                $match->score_extra_1 = $request->input('score_extra_1');
            }
            if ($request->input('score_extra_2')) {
                $match->score_extra_2 = $request->input('score_extra_2');
            }
            if ($request->input('penalties_1')) {
                $match->penalties_1 = $request->input('penalties_1');
            }
            if ($request->input('penalties_2')) {
                $match->penalties_2 = $request->input('penalties_2');
            }
            $match->played_at = $request->input('played_at');
            $match->update();
        }
    }
}
