<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualifier extends Model
{
    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
