<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standing extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($standing) {
            $standing->mp = $standing->w + $standing->d + $standing->l;
            $standing->gd = $standing->gf - $standing->ga;
        });
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
