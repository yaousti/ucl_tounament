<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = [
        'played_at'  => 'date:d M Y'
    ];
    public function team_home()
    {
        return $this->belongsTo(Team::class, 'team_1');
    }

    public function team_away()
    {
        return $this->belongsTo(Team::class, 'team_2');
    }
}
