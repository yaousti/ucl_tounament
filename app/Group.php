<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    public function teams()
    {
        return $this->hasMany(Team::class);
    }
}
