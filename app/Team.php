<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public function groups()
    {
        return $this->belongsTo(Group::class);
    }

    public function qualifiers()
    {
        return $this->hasMany(Qualifier::class);
    }

    public function match_home()
    {
        return $this->hasMany(Match::class, 'team_1');
    }

    public function match_away()
    {
        return $this->hasMany(Match::class, 'team_2');
    }

    public function standings()
    {
        return $this->hasOne(Standing::class);
    }
}
